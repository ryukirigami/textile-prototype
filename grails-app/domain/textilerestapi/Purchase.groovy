package textilerestapi

import grails.rest.Resource

@Resource(uri='/purchase')
class Purchase {

    String noPq
    String noPo
    String warna
    String jenisBahan
    String jumlah
    String harga
    String status
    Customer customer

//    static hasMany = [customer: Customer];

    static constraints = {
        noPo blank: true
        noPq blank: false, unique: true
        warna blank: false
        jenisBahan blank: false
        jumlah blank: false
        harga blank: false
        status blank: true
    }

    String toString(){
        noPo
    }

}
