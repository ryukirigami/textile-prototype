package textilerestapi

import grails.rest.Resource

@Resource(uri='/customer')
class Customer {

    String nama
    String alamat
    String telp
    String ktp
    String npwp

    static constraints = {
        nama blank: false
        alamat blank: true
        telp blank: true
        ktp blank: true
        npwp blank: true
    }

    String toString(){
        nama
    }
}
