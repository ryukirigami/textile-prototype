package textilerestapi


import grails.rest.*
import grails.converters.*

class CustomerController extends RestfulController {
    static responseFormats = ['json', 'xml']
    CustomerController() {
        super(Customer)
    }
}
